'use strict';

$(document).ready(function () {
	
	//------------------------------------------------------------------------------
	// Данные (в идеале грузим картинки с сервера аяксом)
	//------------------------------------------------------------------------------
	var slides = {
			slideShow1: [
							'http://i1.i.ua/prikol/pic/5/0/651505.jpg',
							'http://s00.yaplakal.com/pics/pics_original/2/0/0/1212002.jpg',
							'http://cs403026.vk.me/v403026426/6088/9PIvfUSDN3s.jpg',
							'http://www.zveryshki.ru/uploads/posts/2009-01/1233271562_murr.jpg',
							'http://img0.joyreactor.cc/pics/post/%D0%BA%D0%BE%D1%82%D1%8D-%D1%80%D0%B5%D0%BA%D1%83%D1%80%D1%81%D0%B8%D0%B2%D0%BD%D1%8B%D0%B5-%D0%BA%D0%BE%D1%82%D0%B8%D0%BA%D0%B8-%D0%A0%D0%B5%D0%BA%D1%83%D1%80%D1%81%D0%B8%D1%8F-%D0%BC%D0%B8%D0%BB%D0%BE%D1%82%D0%B0-533097.jpeg'
						],
			slideShow2: [
							'http://i.i.ua/photo/images/pic/8/9/703498_14abc371.jpg',
							'http://os1.i.ua/1/101/133092.jpg',
							'http://megalife.com.ua/uploads/posts/2009-07/1248870233_12.jpg',
							'http://d2.endata.cx/data/games/14191/kotik1.jpg',
							'http://byaki.net/uploads/posts/2008-10/1225141006_1-11.jpg'
						],
			slideShow3: [
							'http://img0.joyreactor.cc/pics/comment/%D0%BB%D0%B8%D1%87%D0%BD%D0%BE%D0%B5-%D0%BA%D0%BE%D1%82%D0%B8%D0%BA%D0%B8-221554.jpeg',
							'http://img0.liveinternet.ru/images/foto/c/9/apps/2/186/2186102_tumblr_l8hzm5eqsg1qc1tbuo1_500_large.png',
							'http://cs7001.vk.me/c540105/v540105628/7c0a/EAOlLDXkDsU.jpg',
							'http://img12.nnm.me/5/e/2/f/9/5e2f93147b79820daa9356cd593579ba_full.jpg',
							'http://bm.img.com.ua/nxs/img/prikol/images/large/3/1/49713_34764.jpg'
						]
		 },
		 images = slides.slideShow1;


	//------------------------------------------------------------------------------
	// Выбор презентации
	//------------------------------------------------------------------------------
	Hammer(document.querySelector('#slideShow1')).on('tap', function () {
		images = slides.slideShow1;
		init();	
	});	

	Hammer(document.querySelector('#slideShow2')).on('tap', function () {
		images = slides.slideShow2;
		init();	
	});	

	Hammer(document.querySelector('#slideShow3')).on('tap', function () {
		images = slides.slideShow3;
		init();	
	});	
		


	//------------------------------------------------------------------------------
	// Методы для отображения слайдов
	//------------------------------------------------------------------------------
	var slide = document.querySelector('#slide'),
		jquerySlide = $('#slide'),
		currentSlide = 0;

	var showSlide = function (index) {
		if ($.isNumeric(index)) {
			jquerySlide.fadeOut('fast', function () {
				jquerySlide.attr('src', images[index]);
			});
			jquerySlide.fadeIn('fast');
		} 
	}

	var next = function () {
		if (currentSlide < slides.slideShow1.length - 1) {
			currentSlide++; 
			showSlide(currentSlide);
		}
	}

	var prev = function () {
		if (currentSlide > 0) {
			currentSlide--;
			showSlide(currentSlide);
		}
	}


	//------------------------------------------------------------------------------
	// Слушатели для кнопок управления
	//------------------------------------------------------------------------------
	Hammer(document.querySelector('#first-slide')).on('tap', function () {
		currentSlide = 0;
		showSlide(currentSlide);
	});

	Hammer(document.querySelector('#next-slide')).on('tap', next);

	Hammer(document.querySelector('#prev-slide')).on('tap', prev);

	Hammer(document.querySelector('#last-slide')).on('tap', function () {
		currentSlide = images.length - 1;
		showSlide(currentSlide);
	});

	Hammer(slide).on('tap', function () {
		if (currentSlide < images.length - 1) {
			currentSlide++; 
			showSlide(currentSlide);
		}
	});

	Hammer(slide).on('swipeleft', function () {
		next();
	});
	Hammer(slide).on('swiperight', function () {
		prev();
	});


	//------------------------------------------------------------------------------
	// Инициализация
	//------------------------------------------------------------------------------

	function init () {
		showSlide(0);
	}

	$('.navbar-collapse a').click(function (e) {
        $('.navbar-collapse').collapse('toggle');
      });

	var navigation = $('#panel-footer');
	setTimeout(function () {
		init();
		navigation.css('display', 'block');
		$('#nav').addClass('my-navigation--hide');
	}, 2000);

});